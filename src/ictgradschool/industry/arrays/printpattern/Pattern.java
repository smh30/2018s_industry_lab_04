package ictgradschool.industry.arrays.printpattern;

public class Pattern {

    public int number;

    public char symbol;


    public Pattern(int number, char symbol) {
        this.number = number;
        this.symbol = symbol;


    }



    public int getNumberOfCharacters() {
        return number;
    }

    public void setNumberOfCharacters(int number) {
        this.number = number;
    }



public String toString(){
    String pattern = "";
    for (int i = 0; i < number; i++) {


        pattern += symbol;
    }
    return pattern;
}
}
