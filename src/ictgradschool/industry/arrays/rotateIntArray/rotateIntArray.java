package ictgradschool.industry.arrays.rotateIntArray;

import ictgradschool.Keyboard;


public class rotateIntArray {

    public void start() {
        String input = getInput();
        int numOfMiniArrays = findOccurrenceOfACharacter(input, '|') + 1;
        String[] smallStrings = breakString(input, numOfMiniArrays);
        int[][] smallInts = convertToInt(smallStrings);
        int[][] rotated = rotateArray(smallInts);
        printOutput(rotated);


    }


    public int findOccurrenceOfACharacter(String input, char targetChar) {
        // TODO write statements here
        int numberOfMiniArrays = 0;
        for (int i = 0; i < input.length(); i++) {
            if (input.charAt(i) == targetChar) {
                numberOfMiniArrays++;
            }
        }

        return numberOfMiniArrays;
    }

    //todo converr input to ints and put them into an array
    //it will be an array made up of multiple other arrays
    public String getInput() {
        System.out.println("Enter arrays, separated by |: ");
        String input = Keyboard.readInput();
        return input;
    }


    //input eg 1, 2, 3|4, 5, 2|3, 1, 7
    //need to break up into substrings
    public String[] breakString(String input, int numOfMiniArrays) {
        String[] smallStrings = new String[numOfMiniArrays];
        int i = 0;
        while (findOccurrenceOfACharacter(input, '|') > 0) {
            smallStrings[i] = input.substring(0, input.indexOf('|'));
            input = input.substring(input.indexOf('|') + 1);
            i++;
        }
        smallStrings[i] = input;
//        System.out.println("smallStrings:");
//        printStringArray(smallStrings);
//        System.out.println("------------------");
        return smallStrings;
    }

    //each small string has commas and spaces in it, need to be removed to form an array of ints
    //and they need to be rotated

    public int[][] convertToInt(String[] smallStrings) {
        int[][] smallInts = new int[smallStrings.length][];

        for (int i = 0; i < smallStrings.length; i++) {
            //convert each string into an array of ints
            smallStrings[i] = smallStrings[i].replaceAll("\\s", "");
            smallStrings[i] = smallStrings[i].replaceAll("\\D", "");

//let's figure out array arrays -_-

            int[] temp = new int[smallStrings[i].length()];
            for (int j = 0; j < smallStrings[i].length(); j++) {
                temp[j] = Integer.parseInt(smallStrings[i].substring(j, j + 1));

            }
            smallInts[i] = temp;


        }
//        printStringArray(smallStrings);
//        printIntArray(smallInts, "smallInts array array");
        return smallInts;
    }



    public int[][] rotateArray(int[][] smallInts) {
        int[][] rotated = new int[smallInts.length][];

        //how to rotate each array
        for (int i = 0; i < smallInts.length; i++) {


            int temp = smallInts[i][0];
            for (int j = 0; j < smallInts[i].length - 1; j++) {
                smallInts[i][j] = smallInts[i][j + 1];
            }

            smallInts[i][smallInts[i].length - 1] = temp;
//            printIntArray(smallInts[i]);

rotated[i]= new int[smallInts[i].length];
            for (int j = 0; j < smallInts[i].length ; j++) {
                rotated[i][j]=smallInts[i][j];
            }



        }
//        printIntArray(rotated, "rotated array");

        return rotated;

    }

    public void printOutput(int[][] outerArray) {
        for (int i = 0; i < outerArray.length - 1; i++) {
            int[] innerArray = outerArray[i];
            for (int j = 0; j < innerArray.length - 1; j++) {
                System.out.print(innerArray[j] + ", ");
            }
            System.out.print(innerArray[innerArray.length - 1] + "|");
        }
        int[] lastArray = outerArray[outerArray.length - 1];
        for (int i = 0; i < lastArray.length - 1; i++) {
            System.out.print(lastArray[i] + ", ");
        }
        System.out.print(lastArray[lastArray.length - 1]);


    }

    public void printIntArray(int[][] array, String message) {
        System.out.println(message + ": ");
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                System.out.println(array[i][j]);
            }
            System.out.println("----");
        }

    }

    public void printStringArray(String[] array) {
        for (int i = 0; i < array.length; i++) {
            System.out.println(array[i].toString());
        }

    }

    public void printIntArray(int[] array) {
        for (int i = 0; i < array.length; i++) {
            System.out.println(array[i]);

        }
        System.out.println("----------");
    }


    public static void main(String[] args) {
        new rotateIntArray().start();
    }

}


