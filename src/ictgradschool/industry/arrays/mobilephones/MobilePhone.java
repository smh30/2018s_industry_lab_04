package ictgradschool.industry.arrays.mobilephones;

public class MobilePhone {

    // TODO Declare the 3 instance variables:
    // brand
    // model
    // price
    public String brand;
    public String model;
    public double price;

    
    public MobilePhone(String brand, String model, double price) {
        // Complete this constructor method
        this.brand = brand;
        this.model = model;
        this.price = price;

    }

    // TODO Uncomment these methods once the corresponding instance variable has been declared.
    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public double getPrice(){
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String toString(){
        return brand + " " + model + " which cost $" + price;
    }


    // TODO Insert isCheaperThan() method here
    public boolean isCheaperThan(Object other) {
        if (other instanceof MobilePhone) {
            MobilePhone otherP = (MobilePhone) other;
            return this.price < otherP.price;
        }
        return false;

    }
    
    // TODO Insert equals() method here
    public boolean equals(Object other){
        if (other instanceof MobilePhone){
            MobilePhone otherP = (MobilePhone)other;
            return this.model.equals(otherP.model)
                    && this.brand.equals(otherP.brand)
                    && this.price == otherP.price;
        }
        return false;
    }

}


